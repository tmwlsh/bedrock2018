<?php
/**
 * Template Name: Contact
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php $contactDetails = get_field('contact_details', 'options'); ?>
<?php $phoneFull = $contactDetails['phone_number']; ?>
<?php $phoneNoSpace = str_replace(' ', '', $phoneFull); ?>
<?php $phoneNumber = str_replace(array( '(', ')' ), '', $phoneNoSpace); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<div class="contact-page">

        <div class="container clearfix">
                <h2>Contact</h2>
                <div class="content-half">
                        <p><?php echo $contactDetails['address']; ?></p>
                        <p><a href="tel:<?php echo $phoneNumber; ?>"><?php echo $contactDetails['phone_number']; ?></a></p>
                        <p><a href="mailto:<?php echo $contactDetails['email_address']; ?>"><?php echo $contactDetails['email_address']; ?></a></p>
                </div>
                <div class="content-half">
                        <?php the_content(); ?>
                </div>
        </div>

        <?php get_template_part( 'modules/content-builder' ); ?>

</div>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
