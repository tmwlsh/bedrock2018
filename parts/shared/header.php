<div class="mobile-menu">
	<div class="container">
		<div class="hamb open">
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
		</div>
		<nav><?php wp_nav_menu( array( 'container_class' => 'primary-nav', 'theme_location' => 'header-menu' ) ); ?></nav>
	</div>
</div>

<?php if(!is_front_page()){ ?>
	<header class="light">
<?php } else { ?>
	<header>
<?php } ?>
	<div class="container clearfix">
		<div class="hamb">
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
		</div>
		<a class="logo" href="<?php echo get_home_url(); ?>">
			<?php include ( get_template_directory() . '/img/inline-bedrock-logo.svg.php') ?>
		</a>
	</div>
</header>
