<footer>
        <?php $contact = get_field('contact_details', 'options'); ?>
        <?php $socials = get_field('socials', 'options'); ?>
        <div class="container clearfix">
                <h2><?php include ( get_template_directory() . '/img/inline-bedrock-logo.svg.php') ?></h2>
                <div class="footer-half">
                        <h3>Contact us:</h3>
                        <ul class="contact">
                                <li><a href="tel:<?php echo $contact['phone_number']; ?>"><?php echo $contact['phone_number']; ?></a></li>
                                <li><a href="mailto:<?php echo $contact['email_address']; ?>"><?php echo $contact['email_address']; ?></a></li>
                        </ul>
                        <ul class="socials horizontal">
                                <li><a href="mailto:<?php echo $socials['linkedin']; ?>"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="mailto:<?php echo $socials['facebook']; ?>"><i class="fab fa-facebook-square"></i></a></li>
                                <li><a href="mailto:<?php echo $socials['twitter']; ?>"><i class="fab fa-twitter"></i></a></li>
                        </ul>
                        <a class="url" href="#">Privacy Notice</a>
                        <a class="url" href="https://www.bedrock-health.com">bedrock-health.com</a>
                </div>
                <div class="footer-half">
                        <p><?php the_field('footer_text', 'options'); ?></p>
                </div>
        </div>
</footer>
