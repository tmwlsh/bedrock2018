(function ($) {
	$(document).ready(function(){

		function homeHeight(){
			var windowHeight = $(window).outerHeight();
			$("div.homepage-header").css("height", windowHeight);
		};

		homeHeight();

		$("header div.hamb").click(function(){
			$(".mobile-menu").toggleClass("open");
		});

		$(".mobile-menu").click(function(){
			$(this).removeClass("open");
		});

		$(".module-text-block").each(function(){
			$("img.icon", this).css("margin-top", 90);
		});


		$(window).scroll(function() {
			if ($(document).scrollTop() >= 100) {
				$('.home header').addClass('scrolled');
			}
			else {
				$('.home header').removeClass('scrolled');
			}
		});

		$(".sweet-block").hover(function(){
			$("h4", this).stop().slideToggle();
		});

		$(".module-text-block-with-background" ).wrapAll('<div class="container background-container clearfix">');

		$('.films-slider').slick({
		  centerMode: true,
		  centerPadding: '0px',
		  slidesToShow: 1,
		});

		function vimeoHeight(){
			var $vimeoWidth = $("iframe.vimeo").outerWidth();
			$("iframe.vimeo").css("height", ($vimeoWidth / (16/9)));
		};

		vimeoHeight();

		// $('.films-slider div.image').css('width', height * 16 / 9);

		$(window).resize(function(){
			homeHeight();
			vimeoHeight();
		});

	});
}(jQuery));
