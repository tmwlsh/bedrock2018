<?php

	add_action( 'init', 'create_post_type_careers' );

	function create_post_type_careers() {
		register_post_type( 'Careers',
		array(
			'labels'             =>
			array(
				'name'               => _x( 'Careers', 'post type general name', 'your-plugin-textdomain' ),
				'singular_name'      => _x( 'Career', 'post type singular name', 'your-plugin-textdomain' ),
				'menu_name'          => _x( 'Careers', 'admin menu', 'your-plugin-textdomain' ),
				'name_admin_bar'     => _x( 'Career', 'add new on admin bar', 'your-plugin-textdomain' ),
				'add_new'            => _x( 'Add New', 'team member', 'your-plugin-textdomain' ),
				'add_new_item'       => __( 'Add New Career', 'your-plugin-textdomain' ),
				'new_item'           => __( 'New Career', 'your-plugin-textdomain' ),
				'edit_item'          => __( 'Edit Career', 'your-plugin-textdomain' ),
				'view_item'          => __( 'View Career', 'your-plugin-textdomain' ),
				'all_items'          => __( 'All Careers', 'your-plugin-textdomain' ),
				'search_items'       => __( 'Search Careers', 'your-plugin-textdomain' ),
				'parent_item_colon'  => __( 'Parent Careers:', 'your-plugin-textdomain' ),
				'not_found'          => __( 'No careers found.', 'your-plugin-textdomain' ),
				'not_found_in_trash' => __( 'No careers found in Trash.', 'your-plugin-textdomain' )
			),
	        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'careers' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor' )
		));
	}

?>
