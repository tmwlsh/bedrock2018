<?php

	add_action( 'init', 'create_post_type_case_studies' );

	function create_post_type_case_studies() {
		register_post_type( 'Case Studies',
		array(
			'labels'             =>
			array(
				'name'               => _x( 'Case Studies', 'post type general name', 'your-plugin-textdomain' ),
				'singular_name'      => _x( 'Case Study', 'post type singular name', 'your-plugin-textdomain' ),
				'menu_name'          => _x( 'Case Studies', 'admin menu', 'your-plugin-textdomain' ),
				'name_admin_bar'     => _x( 'Case Study', 'add new on admin bar', 'your-plugin-textdomain' ),
				'add_new'            => _x( 'Add New', 'team member', 'your-plugin-textdomain' ),
				'add_new_item'       => __( 'Add New Case Study', 'your-plugin-textdomain' ),
				'new_item'           => __( 'New Case Study', 'your-plugin-textdomain' ),
				'edit_item'          => __( 'Edit Case Study', 'your-plugin-textdomain' ),
				'view_item'          => __( 'View Case Study', 'your-plugin-textdomain' ),
				'all_items'          => __( 'All Case Studies', 'your-plugin-textdomain' ),
				'search_items'       => __( 'Search Case Studies', 'your-plugin-textdomain' ),
				'parent_item_colon'  => __( 'Parent Case Studies:', 'your-plugin-textdomain' ),
				'not_found'          => __( 'No case studies found.', 'your-plugin-textdomain' ),
				'not_found_in_trash' => __( 'No case studies found in Trash.', 'your-plugin-textdomain' )
			),
	        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'case-studies' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'thumbnail' )
		));
	}

?>
