<?php

	add_action( 'init', 'create_post_type_memberships' );

	function create_post_type_memberships() {
		register_post_type( 'Memberships',
		array(
			'labels'             =>
			array(
				'name'               => _x( 'Memberships', 'post type general name', 'your-plugin-textdomain' ),
				'singular_name'      => _x( 'Membership', 'post type singular name', 'your-plugin-textdomain' ),
				'menu_name'          => _x( 'Memberships', 'admin menu', 'your-plugin-textdomain' ),
				'name_admin_bar'     => _x( 'Membership', 'add new on admin bar', 'your-plugin-textdomain' ),
				'add_new'            => _x( 'Add New', 'team member', 'your-plugin-textdomain' ),
				'add_new_item'       => __( 'Add New Membership', 'your-plugin-textdomain' ),
				'new_item'           => __( 'New Membership', 'your-plugin-textdomain' ),
				'edit_item'          => __( 'Edit Membership', 'your-plugin-textdomain' ),
				'view_item'          => __( 'View Membership', 'your-plugin-textdomain' ),
				'all_items'          => __( 'All Memberships', 'your-plugin-textdomain' ),
				'search_items'       => __( 'Search Memberships', 'your-plugin-textdomain' ),
				'parent_item_colon'  => __( 'Parent Memberships:', 'your-plugin-textdomain' ),
				'not_found'          => __( 'No memberships found.', 'your-plugin-textdomain' ),
				'not_found_in_trash' => __( 'No memberships found in Trash.', 'your-plugin-textdomain' )
			),
	        	'description'        => __( 'Description.', 'your-plugin-textdomain' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'memberships' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'thumbnail' )
		));
	}

?>
