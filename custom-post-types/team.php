<?php

	add_action( 'init', 'create_post_type_team' );

	function create_post_type_team() {
		register_post_type( 'Team',
		array(
			'labels'             =>
			array(
				'name'               => _x( 'Team Members', 'post type general name', 'your-plugin-textdomain' ),
				'singular_name'      => _x( 'Team Member', 'post type singular name', 'your-plugin-textdomain' ),
				'menu_name'          => _x( 'Team', 'admin menu', 'your-plugin-textdomain' ),
				'name_admin_bar'     => _x( 'Team Member', 'add new on admin bar', 'your-plugin-textdomain' ),
				'add_new'            => _x( 'Add New', 'team member', 'your-plugin-textdomain' ),
				'add_new_item'       => __( 'Add New Team Member', 'your-plugin-textdomain' ),
				'new_item'           => __( 'New Team Member', 'your-plugin-textdomain' ),
				'edit_item'          => __( 'Edit Team Member', 'your-plugin-textdomain' ),
				'view_item'          => __( 'View Team Member', 'your-plugin-textdomain' ),
				'all_items'          => __( 'All Team Members', 'your-plugin-textdomain' ),
				'search_items'       => __( 'Search Team Members', 'your-plugin-textdomain' ),
				'parent_item_colon'  => __( 'Parent Team Members:', 'your-plugin-textdomain' ),
				'not_found'          => __( 'No team members found.', 'your-plugin-textdomain' ),
				'not_found_in_trash' => __( 'No team members found in Trash.', 'your-plugin-textdomain' )
			),
	        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'bedrock-team' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title' )
		));
	}

?>
