<?php
/*
* Module: Text Block With Background
*/
$textBGLink = get_sub_field('text_bg_link');
?>

        <div class="module module-text-block-with-background <?php the_sub_field('width'); ?> clearfix" style="background-image:url('<?php the_sub_field('background_image'); ?>');">
                <div class="container">
                        <div class="content">
                                <h2><?php the_sub_field('block_title'); ?></h2>
                                <p><?php the_sub_field('block_text'); ?></p>
                                <a href="<?php echo $textBGLink['url']; ?>"><?php echo $textBGLink['title']; ?></a>
                        </div>
                </div>
        </div><!-- .module-text-block-with-background -->

<?php
