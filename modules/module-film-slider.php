<?php
/*
* Module: Films Slider
*/

?>

        <div class="module module-films-slider">
                <div class="films-slider">
                        <?php if( have_rows('film_repeater') ): ?>
                                <?php while ( have_rows('film_repeater') ) : the_row(); ?>
                                        <div class="slide">
                                                <a href="#">
                                                        <div class="container">
                                                                <div class="image" style="background-image:url('<?php the_sub_field('film_image'); ?>');"></div>
                                                                <h3><?php the_sub_field('film_title'); ?></h3>
                                                        </div>
                                                </a>
                                        </div>
                                <?php endwhile; ?>
                        <?php endif; ?>
                </div>
        </div><!-- .module-text-block -->

<?php
