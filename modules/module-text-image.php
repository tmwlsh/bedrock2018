<?php
/*
* Module: Team
*/

?>

        <div class="module module-text-image">
                <div class="container">
                        <div class="inner clearfix">
                                <div class="half image <?php the_sub_field('image_position'); ?>">
                                        <img src="<?php the_sub_field('image_field'); ?>" />
                                </div>
                                <div class="half text"><?php the_sub_field('text_field'); ?></div>
                        </div>
                </div>
        </div><!-- .module-text-image -->

<?php
