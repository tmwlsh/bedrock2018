<?php
/*
* Module: Memberships
*/

?>

        <div class="module module-memberships">
                <div class="container clearfix">

                        <h2>Memberships</h2>
                        <?php $args = array( 'post_type' => 'memberships', 'posts_per_page' => 3 ); ?>
                        <?php $loop = new WP_Query( $args ); ?>
                        <div class="membership-container">
                                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                        <div class="membership-block">
                                                <?php the_post_thumbnail(); ?>
                                        </div>
                                <?php endwhile; ?>
                        </div>

                </div>
        </div><!-- .module-memberships -->

<?php
