<?php
/*
* Module: Team
*/

?>

        <div class="module module-team">
                <div class="container">
                        <div class="inner">
                                <h2><?php the_sub_field('team_title'); ?></h2>
                                <?php $args = array( 'post_type' => 'team', 'posts_per_page' => -1 ); ?>
                                <?php $loop = new WP_Query( $args ); ?>
                                        <ul class="clearfix">
                                                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                                        <li class="<?php the_field('width'); ?>">
                                                                <img src="<?php the_field('member_image'); ?>" alt="<?php the_title(); ?>" />
                                                                <h3><?php the_field('role'); ?></h3>
                                                                <h3 class="light"><?php the_title(); ?></h3>
                                                        </li>
                                                <?php endwhile; ?>
                                        </ul>
                        </div>
                </div>
        </div><!-- .module-team -->

<?php
