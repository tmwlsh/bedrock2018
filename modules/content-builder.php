<?php
/*
* Module: Content builder
*/

$acf_id = mbt_acf_id();
if( have_rows('content_builder_modules', $acf_id) ): ?>

        <section class="content-builder">
                <?php while ( have_rows('content_builder_modules', $acf_id) ) : the_row();

                        if( get_row_layout() == 'text_block' ) {
                                get_template_part( 'modules/module', 'text-block' );
                        } elseif( get_row_layout() == 'image_block' ) {
                                get_template_part( 'modules/module', 'image-block' );
                        } elseif( get_row_layout() == 'text_block_with_background' ) {
                                get_template_part( 'modules/module', 'text-block-with-background' );
                        } elseif( get_row_layout() == 'contact_row' ) {
                                get_template_part( 'modules/module', 'contact-row' );
                        } elseif( get_row_layout() == 'team' ) {
                                get_template_part( 'modules/module', 'team' );
                        } elseif( get_row_layout() == 'text_image' ) {
                                get_template_part( 'modules/module', 'text-image' );
                        } elseif( get_row_layout() == 'the_sweet_spot' ) {
                                get_template_part( 'modules/module', 'sweet-spot' );
                        } elseif( get_row_layout() == 'film_slider' ) {
                                get_template_part( 'modules/module', 'film-slider' );
                        } elseif( get_row_layout() == 'case_studies' ) {
                                get_template_part( 'modules/module', 'case-studies' );
                        } elseif( get_row_layout() == 'awards' ) {
                                get_template_part( 'modules/module', 'awards' );
                        } elseif( get_row_layout() == 'memberships' ) {
                                get_template_part( 'modules/module', 'memberships' );
                        } elseif( get_row_layout() == 'video_embed' ) {
                                get_template_part( 'modules/module', 'video-embed' );
                        } elseif( get_row_layout() == 'map' ) {
                                get_template_part( 'modules/module', 'map' );
                        }

                endwhile; ?><!-- .content-builder -->
        </section>

<?php endif;
