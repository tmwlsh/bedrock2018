<?php
/*
* Module: Awards
*/

?>

        <div class="module module-awards">
                <div class="container clearfix">

                        <h2>Awards</h2>

                        <?php if( have_rows('awards_repeater') ): ?>
                                <div class="awards-container">
                                        <?php while ( have_rows('awards_repeater') ) : the_row(); ?>
                                                <div class="award-content">
                                                        <?php the_sub_field('award_content'); ?>
                                                </div>
                                        <?php endwhile; ?>
                                </div>
                        <?php endif; ?>

                </div>
        </div><!-- .module-awards -->

<?php
