<?php
/*
* Module: Contact Row
*/

$contactDetails = get_field('contact_details', 'options');

$phoneFull = $contactDetails['phone_number'];
$phoneNoSpace = str_replace(' ', '', $phoneFull);
$phoneNumber = str_replace(array( '(', ')' ), '', $phoneNoSpace);

?>

        <div class="module module-contact-row">
                <div class="container clearfix">
                        <h3 class="title">Contact <span class="yellow">Bedrock®</span></h3>
                        <div class="half">
                                <img src="<?php bloginfo('template_url'); ?>/img/map.jpg" />
                        </div>
                        <div class="half">
                		<p><?php echo $contactDetails['address']; ?></p>
                		<p><a href="tel:<?php echo $phoneNumber; ?>"><?php echo $contactDetails['phone_number']; ?></a></p>
                		<p><a href="mailto:<?php echo $contactDetails['email_address']; ?>"><?php echo $contactDetails['email_address']; ?></a></p>
                        </div>
                </div>
        </div><!-- .module-contact-row -->

<?php
