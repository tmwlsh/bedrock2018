<?php
/*
* Module: Sweet Spot
*/

?>

        <div class="module module-sweet-spot">
                <div class="container clearfix">
                        <?php if( have_rows('sweet_block_repeater') ): ?>
                                <?php  while ( have_rows('sweet_block_repeater') ) : the_row(); ?>
                                        <?php $blockText = get_sub_field('block_text'); ?>
                                        <?php $blockTitle = get_sub_field('block_title'); ?>
                                        <?php $blockBG = get_sub_field('block_background'); ?>
                                        <div style="background-image: url('<?php the_sub_field('block_background_image'); ?>');" class="<?php the_sub_field('block_background_colour'); ?> sweet-block <?php the_sub_field('block_width'); ?> <?php the_sub_field('block_height'); ?>">
                                                <?php $block2Image = get_sub_field('block_second_background'); ?>
                                                <?php if($block2Image){ ?>
                                                        <div class="sweet-spot-bg-two" style="background-image: url('<?php the_sub_field('block_second_background'); ?>');"></div>
                                                <?php } ?>
                                                <div class="text">
                                                        <?php if($blockTitle){ ?>
                                                                <h3><?php echo $blockTitle; ?></h3>
                                                        <?php } ?>
                                                        <?php if($blockText){ ?>
                                                                <h4><?php echo $blockText; ?></h4>
                                                        <?php } ?>
                                                </div>
                                        </div>
                                <?php endwhile; ?>
                        <?php endif; ?>
                </div>
        </div><!-- .module-sweet-spot -->

<?php
