<?php
/*
* Module: Image Block
*/

?>

        <div class="module module-image-block">
                <div class="container">
                        <div class="inner" style="width:<?php the_sub_field('width'); ?>">
                                <img src="<?php the_sub_field('image'); ?>" />
                        </div>
                </div>
        </div><!-- .module-text-block -->

<?php
