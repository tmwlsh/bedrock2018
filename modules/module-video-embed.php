<?php
/*
* Module: Video Embed
*/

?>

        <div class="module module-video-embed">
                <div class="container">
                        <iframe class="vimeo" src="<?php the_sub_field('video_link'); ?>"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
        </div><!-- .module-video-embed -->

<?php
