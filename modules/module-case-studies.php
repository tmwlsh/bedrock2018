<?php
/*
* Module: Case Studies
*/

?>

        <div class="module module-case-studies">
                <div class="container clearfix">

                        <?php $args = array( 'post_type' => 'casestudies', 'posts_per_page' => 6 ); ?>
                        <?php $loop = new WP_Query( $args ); ?>
                        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                                <div class="case-study-block" style="background-image: url('<?php echo $url; ?>');">
                                        <div class="block-content">
                                                <h3><?php the_title(); ?></h3>
                                                <a href="mailto:freethinking@bedrock-health.com">Find Out More</a>
                                        </div>
                                </div>
                        <?php endwhile; ?>

                </div>
        </div><!-- .module-case-studies -->

<?php
