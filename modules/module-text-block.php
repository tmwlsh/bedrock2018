<?php
/*
* Module: Text Block
*/
$icon = get_sub_field('icon');
?>

        <div class="module module-text-block">
                <div class="container clearfix">
                        <div class="inner" style="width:<?php the_sub_field('width'); ?>">
                                <?php the_sub_field('text'); ?>
                        </div>
                        <?php if($icon){ ?>
                                <img class="icon" src="<?php echo $icon; ?>" />
                        <?php } ?>
                </div>
        </div><!-- .module-text-block -->

<?php
