<?php
	function mbt_acf_id() {
	    if(is_archive()) {
	        $term = get_queried_object();
	        return $term->taxonomy . '_' . $term->term_id;
	    } else {
	        return get_the_id();
	    }
	}
?>
