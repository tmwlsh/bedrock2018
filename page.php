<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<div class="page-container">
        <?php if( is_front_page() ){ ?>
                <div class="homepage-header" style="background-image:url('<?php the_field('homepage_header_background'); ?>');">
                        <div class="container">
                                <div class="home-header-content">
                                        <h1>Imagine. Create. Deliver.</h1>
                                        <h3>Award Winning Medical Communications</h3>
                                </div>
                        </div>
                </div>
        <?php } ?>

        <?php if( is_page( 'procurement' ) ){ ?>

                <?php if ( !post_password_required() ) { ?>
                        <?php get_template_part( 'modules/content-builder' ); ?>
                <?php } else { ?>
                        <div class="password-protected">
                                <div class="module module-text-block">
                                        <div class="container clearfix">
                                                <div class="inner" style="width:70%">
                                                        <h2>Procurement</h2>
                                                        <p>Welcome to the procurement section of our website, for additional detailed information about Bedrock please enter the password previously sent to you below.</p>
                                                        <p>If you have any questions, or would like to request access please email our CEO <a href="mailto:davidyouds@bedrock-healthcare.com">davidyouds@bedrock-healthcare.com</a></p>
                                                </div>
                                                <?php echo get_the_password_form(); ?>
                                        </div>
                                </div>
                        </div>
                <?php } ?>

        <?php } else { ?>

                <?php get_template_part( 'modules/content-builder' ); ?>

        <?php } ?>

</div>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
