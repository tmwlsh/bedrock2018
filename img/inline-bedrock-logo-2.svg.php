<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 107 107" style="enable-background:new 0 0 107 107;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#FFFFFF;}
	.st1{fill:#F3C317;}
	.st2{clip-path:url(#SVGID_3_);fill:#F3C317;}
</style>
<g>
	<path class="st0" d="M39.1,31.8h15.8c3.9,0,6.9,1,8.9,3c1.6,1.6,2.3,3.5,2.3,5.8v0.1c0,1-0.1,1.8-0.4,2.6c-0.2,0.7-0.6,1.4-1,2
		s-0.9,1.1-1.4,1.6s-1.1,0.8-1.7,1.2c2,0.7,3.5,1.8,4.6,3c1.1,1.3,1.7,3.1,1.7,5.3v0.1c0,1.6-0.3,2.9-0.9,4.1
		c-0.6,1.2-1.5,2.1-2.6,2.9s-2.5,1.4-4,1.7c-1.6,0.4-3.3,0.6-5.2,0.6H39.1V31.8z M53.3,45.5c1.7,0,3-0.3,3.9-0.8
		c1-0.6,1.5-1.5,1.5-2.7v-0.1c0-1.1-0.4-2-1.3-2.6c-0.8-0.6-2.1-0.9-3.6-0.9h-7.4v7.2h6.9V45.5z M55.2,59.2c1.7,0,2.9-0.3,3.9-0.9
		c0.9-0.6,1.4-1.5,1.4-2.8v-0.1c0-1.1-0.4-2-1.3-2.7c-0.9-0.7-2.3-1-4.2-1h-8.6v7.5H55.2z"/>
</g>
<g>
	<g>
		<rect id="SVGID_1_" x="39.1" y="71.3" class="st1" width="27.4" height="6.2"/>
	</g>
	<g>
		<defs>
			<rect id="SVGID_2_" x="39.1" y="71.3" width="27.4" height="6.2"/>
		</defs>
		<clipPath id="SVGID_3_">
			<use xlink:href="#SVGID_2_"  style="overflow:visible;"/>
		</clipPath>
		<rect x="39.1" y="71.3" class="st2" width="27.4" height="6.2"/>
	</g>
</g>
</svg>
