<?php echo '<?xml version="1.0" encoding="utf-8"?>'; ?>
<!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 300 107" style="enable-background:new 0 0 300 107;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#F3C317;}
	.st1{clip-path:url(#SVGID_3_);fill:#F3C317;}
	.st2{fill:#FFFFFF;}
</style>
<g>
	<g>
		<rect id="SVGID_1_" x="25.2" y="72" class="st0" width="239.3" height="6.2"/>
	</g>
	<g>
		<defs>
			<rect id="SVGID_2_" x="25.2" y="72" width="239.3" height="6.2"/>
		</defs>
		<clipPath id="SVGID_3_">
			<use xlink:href="#SVGID_2_"  style="overflow:visible;"/>
		</clipPath>
		<rect x="25.2" y="72" class="st1" width="239.3" height="6.2"/>
	</g>
</g>
<g class="letters">
	<path class="st2" d="M25.7,30.5h15.8c3.9,0,6.9,1,8.9,3c1.6,1.6,2.3,3.5,2.3,5.8v0.1c0,1-0.1,1.8-0.4,2.6s-0.6,1.4-1,2
		s-0.9,1.1-1.4,1.6s-1.1,0.8-1.7,1.2c2,0.7,3.5,1.8,4.7,3c1.1,1.3,1.7,3.1,1.7,5.3v0.1c0,1.6-0.3,2.9-0.9,4.1
		c-0.6,1.2-1.5,2.1-2.6,2.9c-1.1,0.8-2.5,1.4-4,1.7c-1.6,0.4-3.3,0.6-5.2,0.6H25.7V30.5z M40,44.3c1.7,0,3-0.3,3.9-0.8
		c1-0.6,1.5-1.5,1.5-2.7v-0.1c0-1.1-0.4-2-1.3-2.6c-0.8-0.6-2.1-0.9-3.7-0.9H33v7.2h7V44.3z M42,58c1.7,0,3-0.3,3.9-0.9
		c0.9-0.6,1.4-1.5,1.4-2.8v-0.1c0-1.1-0.4-2-1.3-2.7c-0.9-0.7-2.3-1-4.2-1H33V58H42z"/>
	<path class="st2" d="M58.9,30.5h25.7v6.7H66.4v6.9h16.1v6.7H66.4v7.1h18.5v6.7h-26V30.5z"/>
	<path class="st2" d="M89.6,30.5h13.3c2.7,0,5.1,0.4,7.3,1.3c2.2,0.9,4.1,2,5.7,3.6c1.6,1.5,2.8,3.3,3.7,5.4
		c0.9,2.1,1.3,4.3,1.3,6.7v0.1c0,2.4-0.4,4.6-1.3,6.7c-0.9,2.1-2.1,3.9-3.7,5.4s-3.5,2.7-5.7,3.6s-4.7,1.3-7.3,1.3H89.6V30.5z
		 M102.9,57.8c1.5,0,2.9-0.2,4.2-0.7c1.3-0.5,2.3-1.2,3.2-2.1c0.9-0.9,1.6-2,2.1-3.2s0.8-2.6,0.8-4.1v-0.1c0-1.5-0.3-2.9-0.8-4.1
		c-0.5-1.3-1.2-2.3-2.1-3.3c-0.9-0.9-2-1.6-3.2-2.1c-1.3-0.5-2.7-0.8-4.2-0.8h-5.8v20.5C97.1,57.8,102.9,57.8,102.9,57.8z"/>
	<path class="st2" d="M125.7,30.5h15.6c4.3,0,7.6,1.1,9.9,3.4c1.9,1.9,2.9,4.5,2.9,7.8v0.1c0,2.8-0.7,5-2,6.7s-3.1,3-5.3,3.8
		l8.3,12.1h-8.8L139,53.5h0h-5.8v10.9h-7.5V30.5z M140.8,47c1.9,0,3.3-0.4,4.3-1.3c1-0.9,1.5-2,1.5-3.5v-0.1c0-1.6-0.5-2.8-1.6-3.6
		c-1-0.8-2.5-1.2-4.3-1.2h-7.4V47H140.8z"/>
	<path class="st2" d="M175.6,65.1c-2.6,0-5-0.5-7.3-1.4c-2.2-0.9-4.1-2.2-5.7-3.7c-1.6-1.6-2.9-3.4-3.7-5.5
		c-0.9-2.1-1.3-4.4-1.3-6.8v-0.1c0-2.4,0.5-4.7,1.4-6.8s2.2-4,3.8-5.6s3.5-2.9,5.7-3.8c2.2-0.9,4.6-1.4,7.3-1.4c2.6,0,5,0.5,7.3,1.4
		c2.2,0.9,4.1,2.2,5.7,3.7c1.6,1.6,2.9,3.4,3.7,5.5c0.9,2.1,1.3,4.4,1.3,6.8v0.1c0,2.4-0.5,4.7-1.4,6.8s-2.2,4-3.8,5.6
		s-3.5,2.9-5.7,3.8C180.6,64.6,178.2,65.1,175.6,65.1z M175.7,58.2c1.5,0,2.9-0.3,4.1-0.8c1.3-0.6,2.3-1.3,3.2-2.3s1.6-2.1,2.1-3.4
		c0.5-1.3,0.8-2.6,0.8-4.1v-0.1c0-1.5-0.3-2.8-0.8-4.1S183.9,41,183,40c-0.9-1-2-1.7-3.3-2.3s-2.6-0.8-4.1-0.8s-2.9,0.3-4.2,0.8
		c-1.3,0.6-2.3,1.3-3.2,2.3s-1.6,2.1-2.1,3.4c-0.5,1.3-0.8,2.6-0.8,4.1v0.1c0,1.5,0.3,2.8,0.8,4.1s1.2,2.4,2.1,3.4
		c0.9,1,2,1.7,3.3,2.3C172.7,57.9,174.1,58.2,175.7,58.2z"/>
	<path class="st2" d="M214.4,65.1c-2.5,0-4.8-0.5-7-1.4c-2.1-0.9-4-2.1-5.6-3.7c-1.6-1.6-2.8-3.4-3.7-5.6c-0.9-2.1-1.3-4.4-1.3-6.8
		v-0.1c0-2.4,0.4-4.7,1.3-6.8c0.9-2.1,2.1-4,3.7-5.6c1.6-1.6,3.4-2.9,5.6-3.8c2.2-0.9,4.6-1.4,7.2-1.4c1.6,0,3,0.1,4.4,0.4
		c1.3,0.3,2.5,0.6,3.6,1.1s2.1,1,3,1.7c0.9,0.6,1.8,1.4,2.5,2.1l-4.8,5.5c-1.3-1.2-2.7-2.1-4.1-2.8s-2.9-1-4.6-1
		c-1.4,0-2.8,0.3-4,0.8c-1.2,0.6-2.3,1.3-3.1,2.3c-0.9,1-1.6,2.1-2,3.4c-0.5,1.3-0.7,2.6-0.7,4.1v0.1c0,1.5,0.2,2.8,0.7,4.1
		s1.2,2.4,2,3.4c0.9,1,1.9,1.7,3.1,2.3c1.2,0.6,2.6,0.8,4,0.8c1.9,0,3.6-0.4,4.9-1.1c1.3-0.7,2.7-1.7,4-2.9l4.8,4.8
		c-0.9,0.9-1.8,1.8-2.7,2.5s-2,1.4-3.1,1.9s-2.3,0.9-3.7,1.2C217.6,65,216.1,65.1,214.4,65.1z"/>
	<path class="st2" d="M232.4,30.5h7.5v14.9l13.8-14.9h9.1l-13.9,14.4l14.5,19.6h-9L243.9,50l-3.9,4.1v10.4h-7.5v-34H232.4z"/>
	<path class="st2" d="M270.3,41.8c-0.8,0-1.5-0.1-2.2-0.4c-0.7-0.3-1.3-0.7-1.8-1.2s-0.9-1.1-1.2-1.8c-0.3-0.7-0.4-1.4-0.4-2.2l0,0
		c0-0.8,0.1-1.5,0.4-2.2s0.7-1.3,1.2-1.8s1.1-0.9,1.8-1.2s1.4-0.5,2.2-0.5c0.8,0,1.5,0.1,2.2,0.4s1.3,0.7,1.8,1.2
		c0.5,0.5,0.9,1.1,1.2,1.8c0.3,0.7,0.4,1.4,0.4,2.2l0,0c0,0.8-0.1,1.5-0.4,2.2s-0.7,1.3-1.2,1.8s-1.1,0.9-1.8,1.2
		S271.1,41.8,270.3,41.8z M270.3,41.2c0.7,0,1.4-0.1,2-0.4c0.6-0.3,1.1-0.6,1.6-1.1c0.4-0.5,0.8-1,1.1-1.6s0.4-1.3,0.4-1.9l0,0
		c0-0.7-0.1-1.3-0.4-1.9s-0.6-1.1-1-1.6s-1-0.8-1.6-1.1c-0.6-0.3-1.3-0.4-2-0.4s-1.4,0.1-2,0.4s-1.1,0.6-1.6,1.1
		c-0.4,0.5-0.8,1-1.1,1.6c-0.3,0.6-0.4,1.3-0.4,1.9l0,0c0,0.7,0.1,1.3,0.4,1.9s0.6,1.1,1,1.6s1,0.8,1.6,1.1
		C269,41.1,269.6,41.2,270.3,41.2z M268,33h2.7c0.8,0,1.4,0.2,1.9,0.7c0.3,0.3,0.5,0.8,0.5,1.3l0,0c0,0.5-0.1,0.9-0.4,1.2
		c-0.2,0.3-0.6,0.5-0.9,0.7l1.5,2.2h-1.5l-1.3-1.9l0,0h-1V39H268V33z M270.7,36c0.3,0,0.6-0.1,0.8-0.2c0.2-0.2,0.3-0.4,0.3-0.6l0,0
		c0-0.3-0.1-0.5-0.3-0.7c-0.2-0.1-0.4-0.2-0.8-0.2h-1.3V36H270.7z"/>
</g>
</svg>
